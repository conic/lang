/// Generate constant sections of transition tabled for symbols
///
/// Tables are written to `src/tables/table_init`
fn main() -> std::io::Result<()> {
    let mut map1 = vec![vec![0; 256], vec![0; 256]];
    let mut map2 = vec![vec![0; 256], vec![0; 256]];
    let mut map3 = vec![vec![0; 256], vec![0; 256]];
    for i in [9, 10, 32, 64, 123, 125] {
        map2[1][i] = 0;
    }
    // (matched string, tt, td)
    let ops = [
        ("+", 2, 0), ("++", 2, 6), ("+=", 5, 0),
        ("-", 2, 1), ("--", 2, 7), ("-=", 5, 1),
        ("*", 2, 3), ("**", 2, 5), ("*=", 5, 3), ("**=", 5, 5),
        ("/", 2, 4), ("/=", 5, 4), ("%", 2, 2), ("%=", 5, 2),
        ("!", 2, 8), ("!=", 3, 1), ("=", 5, 255), ("==", 3, 0),
        ("<", 3, 2), (">", 3, 3), ("<=", 3, 4), (">=", 3, 5),
        (":", 2, 9), ("?", 2, 10), (".", 2, 11),
        ("(", 4, 0), (")", 4, 1), ("[", 4, 2), ("]", 4, 3),
        (",", 2, 12), ("@", 2, 13), ("->", 2, 14), ("=>", 2, 15),
        ("\n", 11, 0), (";", 11, 1)
    ];
    for (s, tt, td) in ops {
        let mut row = 0;
        let s_bytes = s.as_bytes();
        let (last, first) = s_bytes.split_last().unwrap();
        for b in first {
            if map1[row][*b as usize] == 0 {
                map1[row][*b as usize] = map1.len();
                row = map1.len();
                map1.push(vec![0; 256]);
                map2.push(vec![0; 256]);
                map3.push(vec![0; 256]);
            } else {
                row = map1[row][*b as usize];
            }
        }
        map2[row][*last as usize] = tt;
        map3[row][*last as usize] = td;
    }

    for (f, m) in [("map1", map1), ("map2", map2), ("map3", map3)] {
        std::fs::write(format!("src/tables/table_init/{}.in", f), format!("vec!{:?}", m))?;
    }

    Ok(())
}
